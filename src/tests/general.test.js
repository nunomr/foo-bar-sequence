const { getFooBarSequenceValue, getFooBarSequence } = require('../fooBarSequence');

const resultsFrom0To19 = [
  'FooBar',
  '1',
  '2',
  'Foo',
  '4',
  'Bar',
  'Foo',
  '7',
  '8',
  'Foo',
  'Bar',
  '11',
  'Foo',
  '13',
  '14',
  'FooBar',
  '16',
  '17',
  'Foo',
];

describe('general', () => {
  it('check getFooBarSequenceValue output', () => {
    for (let i = 0; i < resultsFrom0To19.length; i += 1) {
      const expectedValue = resultsFrom0To19[i];
      if (expectedValue !== undefined) {
        expect(
          getFooBarSequenceValue(i)
        )
          .toEqual(expectedValue);
      }
    }
  });

  it('check getFooBarSequence output', () => {
    const sequence = [...getFooBarSequence(0, 18)];
    expect(sequence)
      .toEqual(resultsFrom0To19);
  });
});
