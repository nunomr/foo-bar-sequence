const { getFooBarSequence } = require('./fooBarSequence');

console.log(
  [...getFooBarSequence()]
    .join(' ')
);
