function getFooBarSequenceValue(index) {
  if (!Number.isInteger(index)) {
    throw new Error('Invalid index parameter');
  }

  let result = '';
  if (index % 3 === 0) {
    result += 'Foo';
  }
  if (index % 5 === 0) {
    result += 'Bar';
  }
  if (!result) {
    result = String(index);
  }

  return result;
}

function *getFooBarSequence(from = 1, to = 100) {
  if (!Number.isInteger(from)) {
    throw new Error('Invalid from parameter');
  }
  if (!Number.isInteger(to)) {
    throw new Error('Invalid to parameter');
  }

  for (let i = from; i <= to; i += 1) {
    yield getFooBarSequenceValue(i);
  }
}

module.exports = {
  getFooBarSequenceValue,
  getFooBarSequence,
};
