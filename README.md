# Foo bar sequence

A program that prints the numbers from 1 to 100. But for multiples of three print "Foo" instead of the number and for the multiples of five print "Bar". For numbers which are multiples of both three and five print "FooBar”.

> FooBar

Output: 1 2 Foo 4 Bar Foo 7 8 Foo Bar 11 Foo 13 14 FooBar 16 17 Foo ……

## Requirements

- [Node js](https://nodejs.org/en/) (^8.9.4)

Or if you dont want to install node:

- [Docker](https://docs.docker.com/engine/installation/#installation) (^1.10.3)

## Getting started

All commands should be ran from the repository root folder.

### With node js installed

Install the dependencies by running:

```sh
npm install
```

Start the app:

```sh
npm start
```

Run the the tests:

```sh
npm test
```

### With docker installed

If you don't want to install node js and have docker installed you can run the commands from the "With node js installed" section inside a node js docker container.

Start the node js docker container (run from the repository root folder):

```sh
docker run -v `pwd`:`pwd` -w `pwd` -it node:8.9.4 bash
```

Then run the commands from "With node js installed" section.
